package com.springboot.repository;


import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.springboot.beans.Category;


@Repository("categoryRepository")
public interface CategoryRepository extends CrudRepository<Category, Integer>{

	@Query("select c from Category c")
	public List<Category> getAllCategory();
	
	@Query("select c from Category c where c.id = :Id")
	public Category getCategoryById(@Param("Id") int Id);
	

}
