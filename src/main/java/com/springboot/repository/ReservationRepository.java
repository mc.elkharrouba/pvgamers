package com.springboot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springboot.beans.reservation;

@Repository("reservationRepository")
public interface ReservationRepository  extends CrudRepository<reservation, Integer>{
	
	
}
