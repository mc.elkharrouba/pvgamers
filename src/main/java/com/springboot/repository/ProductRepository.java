package com.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.beans.Category;
import com.springboot.beans.Product;

@Repository("productRepository")
public interface ProductRepository extends CrudRepository<Product, Integer > {
	
	@Query("select c from Product c where  c.is_active = 1 AND c.category_id = :Id")
	public List<Product> getAllProductById(@Param("Id") int Id);
	
	
	@Query("select c from Product c where c.is_active = 1 AND c.id = :Id")
	public Product getProductById(@Param("Id") int Id);
	
	@Query("select c from Product c where  c.id = :Id")
	public Product getOnlyProductById(@Param("Id") int Id);
	@Query("select c from Product c")
	public List<Product> getAllProduct();
	
	
	@Query("select c from Product c where  c.is_active = 1 AND c.name LIKE CONCAT('%',:search,'%')")
	public List<Product> getAllProductBySearch(@Param("search") String search);



}
