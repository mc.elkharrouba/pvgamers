package com.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.springboot.beans.Product;
import com.springboot.beans.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
    
    
    @Query("select u from User u where  u.userName = :username")
	public User getOnlyProductById(@Param("username") String username);
}
