package com.springboot.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.springboot.beans.Product;

public class ProducValidation  implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Product.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Product p = (Product) target;
		
		
		if( p.getFile().isEmpty() || p.getFile().getOriginalFilename().equals(" ") || p.getFile().getOriginalFilename().contains("..")) 
		{
			errors.rejectValue("file",null,"Please select file image to upload!");
			return;
		}
		
		if( !( p.getFile().getContentType().equals("image/jpeg") ||  p.getFile().getContentType().equals("image/png") ) ) 
		{
			errors.rejectValue("file",null,"Please use only file image!");
			return;
		}
		
	}
}
	
