package com.springboot.mvc;




import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import com.springboot.repository.UserRepository;

@SpringBootApplication

@EntityScan("com.springboot.beans")
@ComponentScan({"com.springboot.mvc"})
@EnableJpaRepositories(basePackageClasses = UserRepository.class)

public class SpringBootMvcApplication {
	

	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootMvcApplication.class, args);
	      
	}
	
	
	

}
