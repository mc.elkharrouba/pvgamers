package com.springboot.mvc.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springboot.beans.Category;
import com.springboot.beans.Product;
import com.springboot.beans.User;
import com.springboot.beans.reservation;
import com.springboot.repository.CategoryRepository;
import com.springboot.repository.ProductRepository;
import com.springboot.repository.ReservationRepository;
import com.springboot.repository.UserRepository;

@Controller
public class ProductController {

	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired 
	private ReservationRepository reservationRepository;


	@RequestMapping(value="/q_products",method = RequestMethod.GET)
	public ModelAndView getproducts(@RequestParam(value="search" , required = false) String search)
	{
		
		List<Product> product = new ArrayList<Product>();
		
		if( search == null)
		{
			product = productRepository.getAllProduct();
		}
		else
		{
		
		    product = productRepository.getAllProductBySearch(search);
		
		}
	
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("product", product);;
		mv.addObject("myproductsActive","true");;

		return mv;
	}
	
	
	
	@RequestMapping(value="shop/{id}/products")
	public ModelAndView products(@PathVariable("id") int id)
	{
		Category category = categoryRepository.getCategoryById(id);
		
		List<Category> list = categoryRepository.getAllCategory();

		List<Product> product = new ArrayList<Product>();
		product = productRepository.getAllProductById(id);
	
	
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("product", product);
		mv.addObject("category", category);
		mv.addObject("list_category", list);
		mv.addObject("productsActive","true");;
		mv.addObject("title",category.getName());
		return mv;
	}
	
	

	@RequestMapping(value="/product/{id}")
	public ModelAndView product(@PathVariable("id") int id, @RequestParam(value="operation" , required = false) String operation)
	{
		String message = null;
		if(operation != null)
		{
			if(operation.equals("good"))
			{
				message= "Your Booking has been confirmed";
			}
			if(operation.equals("nope"))
			{
				message= "You cannot booking this game twice choose another game !";
			}
		}
		
		Product product = productRepository.getProductById(id);
		Category category = categoryRepository.getCategoryById(product.getCategory_id());
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("productPage","true");
		mv.addObject("title","Shop");
		mv.addObject("product",product);
		mv.addObject("message",message);
		mv.addObject("category",category);
		
		return mv;
	}
	
	@RequestMapping(value="/reserve_game/{p_id}")
	public String Resevation(@PathVariable("p_id") int id)
	{
		
		String typdate = "dd-MM-yyyy";
		String dateInString =new SimpleDateFormat(typdate).format(new Date());
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		
		Product product = productRepository.getProductById(id);
		User user = userRepository.getOnlyProductById(username);
		
		reservation res = new reservation();
		res.setIdu(user.getId());
		res.setIdg(product.getId());
		res.setDate_reservation(dateInString);
		try {
		reservationRepository.save(res);
		
		
			return "redirect:/res/"+String.valueOf(id)+"/";
		}
		catch(Exception ex)
		{
			return "redirect:/product/"+String.valueOf(id)+"?operation=nope";
		}
		
		/*
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("productPage","true");
		mv.addObject("title","Shop");
		mv.addObject("product",product); */
		//return mv;
	}
	
	
	@RequestMapping("/res/{p_id}/")
	public ModelAndView reserves(@PathVariable("p_id") int id)
	{
		String typdate = "dd-MM-yyyy";
		String dateInString =new SimpleDateFormat(typdate).format(new Date());
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		
		Product product = productRepository.getProductById(id);
		User user = userRepository.getOnlyProductById(username);
		
		
		ModelAndView mv = new ModelAndView("productReservation");
		mv.addObject("product",product);
		mv.addObject("user",user);
		mv.addObject("date",dateInString);
		return mv;
		
	}
	
	
	
}
