package com.springboot.mvc.controllers;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.springboot.beans.Category;
import com.springboot.beans.Product;
import com.springboot.beans.User;
import com.springboot.repository.CategoryRepository;
import com.springboot.repository.ProductRepository;
import com.springboot.repository.UserRepository;



/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value = {"/","home","index"})
	public ModelAndView index()
	{
		List<Category> list = new ArrayList<Category>();
		
		list = categoryRepository.getAllCategory();
		
	
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("homeActive","true");
		mv.addObject("title","Home");
		mv.addObject("list",list);
		
		return mv;
	}
	
	@RequestMapping(value="/about")
	public ModelAndView about()
	{

		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "About us");
		mv.addObject("aboutActive","true");
		
		return mv;
	}
	
	@RequestMapping(value="/cart")
	public ModelAndView cart()
	{
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("cartActive","true");
		mv.addObject("title","Cart");
		
		return mv;
	}
	
	@RequestMapping(value="/contact")
	public ModelAndView contact()
	{
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("contactActive","true");
		mv.addObject("title","Contact us");
		
		return mv;
	}
	
	@RequestMapping(value="/shop")
	public ModelAndView shop()
	{
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("shopActive","true");
		mv.addObject("title","Shop");
		
		return mv;
	}
	
	@RequestMapping(value="/product")
	public ModelAndView product()
	{
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("productPage","true");
		mv.addObject("title","Shop");
		
		return mv;
	}
	

	
	
	@RequestMapping(value="/denied")
	public ModelAndView denied()
	{
		ModelAndView mv = new ModelAndView("denied");		
		return mv;
	}
	@RequestMapping("/new_user")
	public ModelAndView newUser(@RequestParam(value="operation" , required = false) String operation)
	{
		String message="";
		if(operation != null)
		{
			if(operation.equals("success"))
			{
				message= "You have successfully registered!";
			}
		}
		Boolean inscriptionActive   = true;
		ModelAndView mv = new ModelAndView("page");
		User user = new User();
		user.setRole("ROLE_USER");
		mv.addObject("user",user);
		mv.addObject("message",message);
		mv.addObject("inscriptionActive",inscriptionActive);
		return mv;
	}
	
	@RequestMapping(value="/new_user/save", method = RequestMethod.POST)
	public String  saveUser(@Valid @ModelAttribute("user")  User u ,  BindingResult result , Model model)
	{
		if(result.hasErrors())
		{
			
			Boolean inscriptionActive = true;
			model.addAttribute("inscriptionActive",inscriptionActive);
			return "page";
		}
		
		
		
		System.out.println(u.getId());
		System.out.println(u.getFirst_name());
		System.out.println(u.getLast_name());
		System.out.println(u.getContact_number());
		System.out.println(u.getEmail());
		System.out.println(u.getPassword());
		System.out.println(u.getEnabled());
		System.out.println(u.getRole());
		userRepository.save(u);
		return "redirect:/new_user?operation=success";
	}
	
	


}
