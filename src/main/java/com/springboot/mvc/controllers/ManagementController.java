package com.springboot.mvc.controllers;
import java.io.BufferedOutputStream;
import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.springboot.beans.Category;
import com.springboot.beans.Product;
import com.springboot.repository.CategoryRepository;
import com.springboot.repository.ProductRepository;
import com.springboot.validation.ProducValidation;


@Controller
public class ManagementController {
	
	@Autowired
	private CategoryRepository CategoryRepository;
	
	@Autowired 
	private ProductRepository productRepository;
	
	@RequestMapping(value="/admin")
	public ModelAndView admin()
	{
		
		ModelAndView mv = new ModelAndView("manager");	
		return mv;
		
	}
	
	@ModelAttribute("category")
	public List<Category> getCategoet(){
		return CategoryRepository.getAllCategory();
	}
	
	@RequestMapping(value="/admin/saveproduct", method = RequestMethod.POST  )
	public String insertData(@Valid @ModelAttribute("product") Product nproduct , BindingResult result , Model model , @RequestParam("file") MultipartFile file ) throws FileNotFoundException
	{
		
		new ProducValidation().validate(nproduct, result);
		
		if(result.hasErrors())
		{
			Boolean addProductActive = true;
			model.addAttribute("addProductActive",addProductActive);
			return "manager";
		}
	
		
		String uploadDirectory = System.getProperty("user.dir")+"/src/main/resources/static/img";
		//String uploadDirectory = request.getServletContext().getRealPath(pathfolder);
		System.out.print("uploadDirectory :" + uploadDirectory);
		String FileName = file.getOriginalFilename();
		String FilePath = Paths.get(uploadDirectory, FileName).toString();
		System.out.print("FilePath :" + FilePath);
		
		/**
		File Dir = new File(uploadDirectory);
		if(!Dir.exists())
		{
			Dir.mkdirs();
		}**/
		
		try {
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(FilePath)));
			stream.write(file.getBytes());
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		nproduct.setImageURL(FileName); 
		productRepository.save(nproduct);
		return "redirect:/admin/addproduct?operation=scuess";
	}
	
	@RequestMapping("/admin/addproduct")
	public ModelAndView productPage(@RequestParam(value="operation" , required = false) String operation)
	{
		String message = null;
		if(operation != null)
		{
			if(operation.equals("scuess"))
			{
				message= "Game added to list successfully";
			}
		}
		
		Product product = new Product();
		product.setIs_active(true);
		product.setSupplier_id(2);
		Boolean addProductActive = true;
		ModelAndView mv = new ModelAndView("manager");
		mv.addObject("addProductActive",addProductActive);
		mv.addObject("message",message);
		mv.addObject("product",product);
		
		return mv;
	}
	
	
	@RequestMapping("/admin/updating/{id}/product")
	public ModelAndView productPage(@PathVariable("id") int id)
	{
	
		Product p = new Product();
		p = productRepository.getProductById(id);
		Boolean updateproductActive = true;
		ModelAndView mv = new ModelAndView("manager");
		mv.addObject("updateproductActive",updateproductActive);
		mv.addObject("product",p);
		
		return mv;
	}
	
	
	
	@RequestMapping("/admin/addCategory")
	public ModelAndView categoryPage()
	{
		Boolean addCategoryActive = true;
		ModelAndView mv = new ModelAndView("manager");
		mv.addObject("addCategoryActive",addCategoryActive);

		return mv;
		
	}
	
	@RequestMapping("/admin/viewallproduct")
	public ModelAndView allproduct() 
	{
		List<Product> listproduct = new ArrayList<Product>();
		listproduct = productRepository.getAllProduct();
		boolean viewallproductActive = true;
		ModelAndView mv = new ModelAndView("manager");
		mv.addObject("listProduct",listproduct);
		mv.addObject("viewallproductActive",viewallproductActive);
		return mv;
	}
	
	@RequestMapping("/admin/viewallres")
	public ModelAndView allres() 
	{
		List<Product> listproduct = new ArrayList<Product>();
		listproduct = productRepository.getAllProduct();
		boolean viewallproductActive = true;
		ModelAndView mv = new ModelAndView("manager");
		mv.addObject("listProduct",listproduct);
		mv.addObject("viewallproductActive",viewallproductActive);
		return mv;
	}
	
	@RequestMapping("/admin/viewallproductadmin/activite/{id}/product")
	@ResponseBody
	public String getActivited(@PathVariable("id") int id)
	{
		Product p = new Product();
		p = productRepository.getOnlyProductById(id);
		
		String message=  null  ;
		if(p.getIs_active())
		{
			p.setIs_active(false);
			productRepository.save(p);
			message="The product is Disabled now!";
			
		}
		else
		{
			p.setIs_active(true);
			productRepository.save(p);
			message="The product is enbled now!";
		}
		
		return message;
	}
	
	
}


