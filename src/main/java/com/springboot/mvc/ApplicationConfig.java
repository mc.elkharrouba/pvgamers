package com.springboot.mvc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.XmlViewResolver;

@Configuration
@ComponentScan(basePackages = "com.springboot.mvc")
public class ApplicationConfig extends WebMvcConfigurationSupport {
	
	@Override
	    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
	        registry.addResourceHandler("css/**", "img/**","js/**","jquery/**")
	                .addResourceLocations("classpath:/static/css/", "classpath:/static/img/","classpath:/static/js/","classpath:/static/jquery/");
	    }

	
	    @Bean
	    public InternalResourceViewResolver jspViewResolver(){
	        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	        viewResolver.setPrefix("/WEB-INF/views/");
	        viewResolver.setSuffix(".jsp");
	        viewResolver.setViewClass(JstlView.class);
	        return viewResolver;
	    }
	    
	  /*  
	    @Bean 
	    public XmlViewResolver xmlViewResolver()
	    {
	    	XmlViewResolver xml = new XmlViewResolver();
	    	xml.setLocation(new ClassPathResource("bean.xml"));
	    	
	    	return xml;
	    } */
}
