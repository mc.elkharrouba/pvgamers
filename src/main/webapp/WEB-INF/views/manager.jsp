<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@  taglib uri="http://www.springframework.org/tags" prefix="spring"  %>
<%@ page session="false" %>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Final Gadget - ${title}</title>
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/style.css" rel="stylesheet">
  
  <script>
  	
  		window.menu = '${title}' ;
  		
  </script>
</head>

<body>

	
  <!-- Navbar -->
  	<%@ include file="./admin/navbar.jsp" %>
  	
  	
  	
  <!-- End Navbar -->
  
    <c:if test="${addProductActive == true}">
   	<%@ include file="./admin/viewallproduct.jsp" %>
    </c:if>
  <c:if test="${addProductActive == true}">
  	<%@ include file="./admin/addProduct.jsp" %>
  </c:if>	
  	
  <c:if test="${ addCategoryActive == true }">
  	<%@ include file="./admin/addCategory.jsp" %>
  </c:if>	
  
  <c:if test="${ viewallproductActive == true}">
  	<%@ include file="./admin/viewallproduct.jsp" %>
  </c:if>
  
  <c:if test="${ updateproductActive == true}">
  	<%@ include file="./admin/updateProduct.jsp" %>
  </c:if>
    <!-- End container -->


  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
  <script src="/js/mini.js" ></script>
</body>

</html>
