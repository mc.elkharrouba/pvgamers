<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	 <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a class="current" href="${contextRoot}/home">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">all Products </li>
                      <li class="search ml-auto"  >
                      	<form action="/q_products" method="get">
                      	<input type="text"  name="search" class="input-text" placeholder="search for product..">
                      	<input type="submit" class="input-btn" value="search">
                      	</form>
                      </li>
                    </ol>

                  </nav>
                	
                	<br>
                	<br>
                	
                 
                  	<br>
                	<br>
                  <div class="conent-products">
                          <div class="container">
                                        <div class="row">
                                                  
                                                      <div class="col-md-12 col-lg-12 ">
                                                                <div class="container">
                                                                    <!-- Just an image -->
                                                                         
                                
                                                                                <div class="row row-cols-1 row-cols-md-3">
                                                                          	   		   <c:forEach items="${product}" var="p">
                                                                          	   		   
                                                                                            <div class="col mb-4">
                                                                                           			<a href="/product/${p.id}" class="a-link"> 
                                                                                                      <div class="card h-100 ">
                                                                                                                      
                                                                                                                      <img src="/img/${p.imageURL}" height="240px" width="20%" class="card-img-top" alt="..." >
                                                                                     
                                                                                                                      <div class="card-body text-center">
                                                                                                                                <h5 class="card-title">${p.name}</h5>
                                                                                                                                <p class="card-text text-center">${p.unit_price}</p>
                                                                                                                      </div>
                                                                                                      </div>
                                                                                            		</a> 
                                                                                            </div>
                                                                                       </c:forEach>
                                                                                </div>           
                                                                      
                                                                </div>
                                                  </div>
                           				 </div>
                		</div>
                </div>
