<div class="upper-nav">
								<div class="container">
									<div class="row">
									    <div class="col-sm text-sm-left">
									       <i class="fas fa-phone-alt">&nbsp;</i><span>(432)782-1820</span>&nbsp;&nbsp;&nbsp;
									       <i class="far fa-envelope"></i>&nbsp;<span>pvgames@gmail.com</span>
									    </div>
									    <div class="col-sm sm text-sm-right text-right">
									      <a href="${contextRoot}/new_user"><span>Sign Up</span></a>&nbsp;&nbsp;&nbsp;
									      <a href="${contextRoot}/login"><span class="signin">Sign in</span></a>
									    </div>
									</div>
								</div>
							</div>
								
								<div class="second-nav">
								<div class="container">
									<nav class="navbar navbar-expand-lg navbar-light ">
									  <a class="navbar-brand" href="${contextRoot}/home">PV<span class="gadget-logo">Gamer</span></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									    <ul class="navbar-nav ml-auto">
									      <li class="nav-item" id="home">
									        <a class="nav-link" href="${contextRoot}/home">Accueil</a>
									      </li>
									      <li class="nav-item" id="shop">
									        <a class="nav-link" href="${contextRoot}/shop/1/products">Reserve</a>
									      </li>
									      <li class="nav-item" id="about">
									        <a class="nav-link" href="${contextRoot}/q_products">all games</a>
									      </li>
									     
									    </ul>
									  </div>
									</nav>
								</div>
</div>