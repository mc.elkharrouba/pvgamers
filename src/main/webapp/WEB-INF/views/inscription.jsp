<%@taglib uri="http://www.springframework.org/tags/form" prefix="cf" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	

  	<div class="container">
  	<c:if test="${not empty message}">
		<div class="alert alert-success" role="alert" id="message">${message}</div>
	</c:if>
  			<div class="card bg-light mb-3 " >
				  <div class="card-header text-center">
				  	<h4>Inscription</h4>
				  </div>
				  <div class="card-body">
				  	
				  		<cf:form method="post" modelAttribute="user" action="/new_user/save" >
				  				  <div class="form-group row">
									    <label for="inputEmail3" class="col-sm-2 col-form-label">Nom :</label>
									    <div class="col-sm-10">
									      	<cf:input type="text" path="first_name" class="form-control" id="inputEmail3"/>
									      	<cf:errors cssClass="invalid-feedback" name="first_name" element="div"></cf:errors>  
									    </div>
								  </div>
								  
								  <div class="form-group row">
									    <label for="inputEmail3" class="col-sm-2 col-form-label">Pr�nom :</label>
									    <div class="col-sm-10">
									      	<cf:input type="text" path="last_name" class="form-control" id="inputEmail3"/>
									      	<cf:errors cssClass="invalid-feedback" name="last_name" element="div"></cf:errors>  
									    </div>
								  </div>
								  
								  
								  
								  <div class="form-group row">
									    <label for="inputEmail3" class="col-sm-2 col-form-label">N� de t�lephone :</label>
									    <div class="col-sm-10">
									      	<cf:input type="text" path="contact_number" class="form-control" id="inputEmail3"/>
									      	<cf:errors cssClass="invalid-feedback" name="contact_number" element="div"></cf:errors>  
									    </div>
								  </div>
								  
								  
								  <div class="form-group row">
									    <label for="inputEmail3" class="col-sm-2 col-form-label">Email :</label>
									    <div class="col-sm-10">
									      	<cf:input type="text" path="email" class="form-control" id="inputEmail3"/>
									      	<cf:errors cssClass="invalid-feedback" name="email" element="div"></cf:errors>  
									    </div>
								  </div>
								  <div class="form-group row">
									    <label for="inputEmail3" class="col-sm-2 col-form-label">Username :</label>
									    <div class="col-sm-10">
									      	<cf:input type="text" path="userName" class="form-control" id="inputEmail3"/>
									      	<cf:errors cssClass="invalid-feedback" name="userName" element="div"></cf:errors>  
									    </div>
								  </div>
								  
								  <div class="form-group row">
									    <label for="inputEmail3" class="col-sm-2 col-form-label">Mot de passe :</label>
									    <div class="col-sm-10">
									      	<cf:input type="text" path="password" class="form-control" id="inputEmail3"/>
									      	<cf:errors cssClass="invalid-feedback" name="password" element="div"></cf:errors>  
									    </div>
								  </div>
								  
								  <div class="form-group row">
									  	  <div class="col-sm-10">
									   		 <cf:hidden path="id"/>
									     	 <cf:hidden path="role"/>
									      	 <cf:hidden path="enabled"/> 
									      <button type="submit" class="btn btn-primary">submit</button>
									   	  </div>
								  </div>
				  		
				  		</cf:form>
								 
				  </div>
  	
 		 	</div>

	</div>

  