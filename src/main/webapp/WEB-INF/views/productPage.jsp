<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
				 <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a class="current" href="${contextRoot}/home">Home</a></li>
                      <li class="breadcrumb-item " aria-current="page"><a class="current" href="${contextRoot}/shop/${category.id}/products">${category.name}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">${product.name}</li>
                    </ol>

                  </nav>
                  <div id="content">
                  <main class="container-page">
                  
                  	<c:if test="${not empty message}">
						<div class="alert alert-success" role="alert" id="message">${message}</div>
					</c:if>
 
                    <!-- Left Column / Headphones Image -->
                    <div class="left-column">
                      <img data-image="black" src="/img/${product.imageURL}" alt="">
                    </div>
                   
                   
                    <!-- Right Column -->
                    <div class="right-column">
                   
                      <!-- Product Description -->
                      <div class="product-description">
                        <span>${category.name }</span>
                        <h1>${product.name}</h1>
                        <p>${product.description}</p>
                      </div>
                   
                      <!-- Product Configuration -->
                      <div class="product-configuration">
                   
                        <!-- Product Color -->
                        <div class="product-color">
                          <span>Location :</span>
                          
                        
                   
                          <div class="color-choose" id="map">
                            
                          </div>
                   
                        </div>
                   
              
                   
                      <!-- Product Pricing -->
                      <div class="product-price">
                        <span>${product.unit_price} $</span>
                        <a href="/reserve_game/${product.id}" class="cart-btn">R�SERVE JEUX !</a>
                      </div>
                    </div>
                  </main>
                  </div>
                  
                   <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: 45.49439779457518, lng: -73.59953641891481};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
