<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@  taglib uri="http://www.springframework.org/tags" prefix="spring"  %>
<%@ page session="false" %>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PVGamers - ${title}</title>
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/style.css" rel="stylesheet">
  
  <script>
  		var key = "AIzaSyC-GVeC_6yIG0NYjw-LFikL5av0HXObKZ8";
  		window.menu = '${title}' ;
  		
  </script>
</head>

<body>

  <!-- Navbar -->
  	<%@ include file="./shared/navbar.jsp" %>
  <!-- End Navbar -->
  
  
  <!---- Page Content ---->
  <c:if test="${homeActive == true }">
	<%@include file="./home.jsp" %>
   </c:if>
   
   <c:if test="${aboutActive == true }">
   	<%@include file="./about.jsp" %>
   </c:if>
   
   <c:if test="${cartActive == true}">
   	<%@include file="./cart.jsp" %>
   </c:if>
   
   <c:if test="${contactActive == true}">
   	<%@include file="./contact.jsp" %>
   </c:if>
   
   <c:if test="${shopActive == true }">
   	<%@include file="./shop.jsp" %>
   </c:if>
   
   <c:if test="${productsActive == true }" >
   	<%@include file="./products.jsp" %>
   </c:if>
   
   <c:if test="${productPage == true }" >
   	<%@include file="./productPage.jsp" %>
   </c:if>
   <c:if test="${myproductsActive == true }" >
   	<%@include file="./allproducts.jsp" %>
   </c:if>
   <c:if test="${inscriptionActive == true }">
	<%@include file="./inscription.jsp" %>
   </c:if>
  <!-- End Page Content -->
  
  
  <!------ Footer ------>	
	<%@ include file="./shared/footer.jsp" %>
	
    <!-- End container -->


  <!-- Bootstrap core JavaScript -->
  <script src="/jquery/jquery.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>
  
 	<script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-GVeC_6yIG0NYjw-LFikL5av0HXObKZ8&callback=initMap">
    </script>
  <script src="/js/mini.js" ></script>
</body>

</html>
