<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	 <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a class="current" href="${contextRoot}/home">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">${category.name}</li>
                      <li class="search ml-auto"  >
                      	<form action="/q_products" method="get">
                      	<input type="text"  name="search" class="input-text" placeholder="search for product..">
                      	<input type="submit" class="input-btn" value="search">
                      	</form>
                      </li>
                    </ol>

                  </nav>
                  <div class="slider_" >
                    <div class="container-fluied">
                                <div class="content">
                                      <div class="context">
                                          <h2>${category.name}</h2>
                                          <p>${catotegory.description}</p>
                                      </div>
                                      <div class="content-overly"></div>
                                      <div class="conent-image" style="background-image: url('/img/${category.image_url}');">
                                      </div>
                                </div>
                    </div>            
                  </div>

                  <div class="conent-products">
                          <div class="container">
                                        <div class="row">
                                                      <div class="col-md-3 col-lg-3 ">
                                                              <div class="container">
                                                                      <h4 class="text-center sidebar-left-title">CATEGORY</h4>
                                                                      
                                                                      <div class="nav flex-column nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                                      	<c:forEach  items="${list_category}" var="list">
                                                                      		
                                                                        		<a class="nav-link <c:if test='${list.name == category.name}'>active</c:if>"  href="${contextRoot}/shop/${list.id}/products" role="tab" aria-controls="v-pills-home" aria-selected="true">${list.name}</a>
                                                                        	
                                                                        </c:forEach>
                                                                      </div>
                                                              </div>
                                                      </div>
                                                      <div class="col-md-9 col-lg-9 ">
                                                                <div class="container">
                                                                    <!-- Just an image -->
                                                                              <nav class="navbar navbar-light bar-content-card">
                                                                                        <div class="dropdown  mr-auto">
                                                                                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                  BEST MATCH
                                                                                                </a>
                                                                                        
                                                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                                                  <a class="dropdown-item" href="#">BEST SELLING</a>
                                                                                                  <a class="dropdown-item" href="#">Another action</a>
                                                                                                  <a class="dropdown-item" href="#">Something else here</a>
                                                                                                </div>
                                                                                        </div>
                                      
                                                                                        <div class="pages">
                                                                                          <p>Show 01- 09 Of 36 Product</p>                                             
                                                                                        </div>
                                      
                                                                              </nav>
                                
                                                                                <div class="row row-cols-1 row-cols-md-3">
                                                                          	   		   <c:forEach items="${product}" var="p">
                                                                          	   		   
                                                                                            <div class="col mb-4">
                                                                                           			<a href="/product/${p.id}" class="a-link"> 
                                                                                                      <div class="card h-100 ">
                                                                                                                      
                                                                                                                      <img src="/img/${p.imageURL}" height="240px" width="20%" class="card-img-top" alt="..." >
                                                                                     
                                                                                                                      <div class="card-body text-center">
                                                                                                                                <h5 class="card-title">${p.name}</h5>
                                                                                                                                <p class="card-text text-center">${p.unit_price}</p>
                                                                                                                      </div>
                                                                                                      </div>
                                                                                            		</a> 
                                                                                            </div>
                                                                                       </c:forEach>
                                                                                </div>           
                                                                      
                                                                </div>
                                                  </div>
                           				 </div>
                		</div>
                </div>
