<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form"  prefix="cf"%>
<div class="container">
	<h1>Available Products</h1>
	<hr/>

		
		
			<table id="productsTable"  class="table table-condensed table-bordered">
							
				<thead>					
					<tr>					
						<th>Id</th>
						<th>&#160;</th>
						<th>Name</th>
						<th>Publisher</th>
						<th>Qty. Avail</th>
						<th>Unit Price</th>
						<th>Activate</th>				
						<th>Edit</th>
					</tr>					
				</thead>
				<c:forEach items="${listProduct}"  var="list">
					<c:if test="${list.is_active == true}">
						<c:set var="checked" value="checked"></c:set>
					</c:if>
					<c:if test="${list.is_active == false}">
						<c:set var="checked" value="unchecked"></c:set>
					</c:if>
					<tr>
					<td>${list.id}</td>
					<td>
						<img alt="" height="100px" width="100px" src="/img/${list.imageURL}">
					</td>
					<td>
						${list.name}
					</td>
					<td>
						${list.publisher}
					</td>
					<td>
						${list.quantity}
					</td>
					<td>
						${list.unit_price}$
					</td>
					<td >
						<label class="switch">
						  <input type="checkbox"  value="${list.id}" ${checked}>
						  <span class="sliders round"></span>
						</label>
												
					</td>
					<td>
						<button type="button" class="btn btn-warning btn-edit" value="${list.id}" data-toggle="modal" data-target="#exampleModal" href="/admin/updating/${list.id}/product">Edit</button>
					</td>
					</tr>
				</c:forEach>
				 <tfoot>
					<tr>					
						<th>Id</th>
						<th>&#160;</th>
						<th>Name</th>
						<th>publisher</th>
						<th>Qty. Avail</th>
						<th>Unit Price</th>
						<th>Activate</th>				
						<th>Edit</th>
					</tr>									
				</tfoot>
				
				<!-- Modal -->
		
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary">Save changes</button>
					      </div>
					    </div>
					  </div>
					</div>
							
			</table>
		
		
</div>
</div>