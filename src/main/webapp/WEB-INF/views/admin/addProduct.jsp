<%@taglib uri="http://www.springframework.org/tags/form" prefix="cf" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container">
	
	<c:if test="${not empty message}">
		<div class="alert alert-success" role="alert" id="message">${message}</div>
	</c:if>
	<div class="card bg-light mb-3 " >
		  <div class="card-header text-center">
		  	<h4>Product Management </h4>
		  </div>
		  <div class="card-body">
					  	<cf:form method="post" modelAttribute="product" action="/admin/saveproduct"  enctype="multipart/form-data">
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Enter Game Name :</label>
						    <div class="col-sm-10">
						      <cf:input type="text" path="name" class="form-control" id="inputEmail3" />
						       <cf:errors cssClass="invalid-feedback" path="name" element="div"></cf:errors>
						      
						    </div>
						  </div>
						  
	
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Enter Game Publisher :</label>
						    <div class="col-sm-10">
						      <cf:input type="text" path="publisher" class="form-control" id="inputEmail3" />
						      <cf:errors cssClass="invalid-feedback" path="publisher" element="div"></cf:errors>
						      
						    </div>
						  </div>
						  
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Game description :</label>
						    <div class="col-sm-10">
						        <cf:textarea class="form-control" path="description" id="exampleFormControlTextarea1" rows="3" />
						         <cf:errors cssClass="invalid-feedback" path="description" element="div"></cf:errors>
						    </div>
						  </div>
						  
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Enter Unit Price :</label>
						    <div class="col-sm-10">
						      <cf:input type="text" path="unit_price" class="form-control" id="inputEmail3" />
						       <cf:errors cssClass="invalid-feedback" path="unit_price" element="div"></cf:errors>
						    </div>
						  </div>
						  
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Quantity  available:</label>
						    
						    <div class="col-sm-10">
						      <cf:input type="text" path="quantity" class="form-control" id="inputEmail3" />
						       <cf:errors cssClass="invalid-feedback" path="quantity" element="div"></cf:errors>
						    </div>
						  </div>
						  
						   <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">select Image :</label>
						    <div class="col-sm-10">
						      <div class="custom-file">
							    <cf:input type="file" path="file"  />
							   <!--  <label class="custom-file-label" for="inputGroupFile03">Choose file</label>  -->
							  </div>
						      <cf:errors cssClass="invalid-feedback" path="file" element="div"></cf:errors>
						    </div>
						  </div>
						  
						   <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">select category :</label>
						    <div class="col-sm-10">
						       <cf:select class="custom-select" path="category_id" id="inputGroupSelect01" items="${category}"  itemLabel="name" itemValue="id">
							    
							  </cf:select>
						      
						    </div>
						  </div>
						  
						   <div class="form-group row">
						    <div class="col-sm-10">
						      <cf:hidden path="id"/>
						     	 <cf:hidden path="purchases"/>
						       <cf:hidden path="views"/>
						        <cf:hidden path="code"/>
						         <cf:hidden path="imageURL"/>
						         <cf:hidden path="supplier_id"/>
						      <button type="submit" class="btn btn-primary">submit</button>
						    </div>
						  </div>
						 </cf:form>
		  </div>
	</div>

</div>